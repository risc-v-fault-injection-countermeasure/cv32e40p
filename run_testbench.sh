#!/bin/bash

set -x
set -e

DIR="obj_dir"
CORE="cv32e40p"
PACKAGE="${CORE}_pkg"
MODULE="${CORE}_${1}"
TESTBENCH="tb_${MODULE}.cpp"

# Move into the directory containing the RTL model
cd rtl

# Lint with all warnings enabled, but continue if warnings occur
set +e
verilator -Wall --trace --lint-only  "include/${PACKAGE}.sv" "${MODULE}.sv"
set -e

# Generate header files
verilator --Wno-lint --Wno-UNOPTFLAT --trace --cc "include/${PACKAGE}.sv" "${MODULE}.sv"

# Generate Makefile(s)
verilator --Wno-lint --Wno-UNOPTFLAT --trace --x-assign unique --x-initial unique --cc "include/${PACKAGE}.sv" "${MODULE}.sv" --exe "${TESTBENCH}"

# Build the simulation executable
make -C "${DIR}" -f "V${PACKAGE}.mk" "V${PACKAGE}"

# Run the testbench
"./obj_dir/V${PACKAGE}" +verilator+rand+reset+2 

# View the waveforms
gtkwave "./waveform.vcd"
