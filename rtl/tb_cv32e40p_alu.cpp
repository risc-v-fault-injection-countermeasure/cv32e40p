#include <stdlib.h>
#include <iostream>
#include <verilated.h>
#include <verilated_vcd_c.h>
#include <Vcv32e40p_pkg.h>

// Simulate 100 clock edges
#define MAX_SIM_TIME 100
vluint64_t sim_time = 0;
vluint64_t posedge_cnt = 0;

int main(int argc, char** argv, char** env) {
  Verilated::commandArgs(argc, argv);

  // Instantiate the adder module
  Vcv32e40p_pkg *dut = new Vcv32e40p_pkg;

  // Dump waveforms
  Verilated::traceEverOn(true);
  VerilatedVcdC *m_trace = new VerilatedVcdC;
  dut->trace(m_trace, 2);
  m_trace->open("waveform.vcd");

  // Simulate
  while (sim_time < MAX_SIM_TIME) {
    dut->operand_c_i = 0;
    dut->enable_i = 1;
    dut->vector_mode_i = 0;
    dut->bmask_a_i = 0;
    dut->bmask_b_i = 0;
    dut->imm_vec_ext_i = 0;
    dut->is_clpx_i = 0;
    dut->is_subrot_i = 0;
    dut->clpx_shift_i = 0;
    dut->ex_ready_i = 1;

    // Reset in cycles 2-5
    if (sim_time > 3 && sim_time <= 6) {
      dut->rst_n = 0;
    } else {
      dut->rst_n = 1;
    }

    // add 3 + 4 = 7
    if (sim_time % 7 == 0) {
      dut->operator_i = 0b0011000;
      dut->operand_a_i = 3;
      dut->operand_b_i = 4;

      // check: add 10 + 5 = 15
      dut->check_op_group_i = 3;
      dut->check_operator_i = 0b001100;
      dut->check_operand_a_i = 10;
      dut->check_operand_b_i = 5;
      dut->check_result_i = 15;
    }

    // sub 9 - 1 = 8
    if (sim_time % 7 == 1) {
      dut->operator_i = 0b0011001;
      dut->operand_a_i = 9;
      dut->operand_b_i = 1;

      // check previous instruction
      dut->check_op_group_i = 3;
      dut->check_operator_i = 0b0011000;
      dut->check_operand_a_i = 3;
      dut->check_operand_b_i = 4;
      dut->check_result_i = 7;
    }

    // sub -20 - -10 = -10
    if (sim_time % 7 == 2) {
      dut->operator_i = 0b0011001;
      dut->operand_a_i = -20;
      dut->operand_b_i = -10;

      // check previous instruction
      dut->check_op_group_i = 3;
      dut->check_operator_i = 0b0011001;
      dut->check_operand_a_i = 9;
      dut->check_operand_b_i = 1;
      dut->check_result_i = 8;
    }

    // Shift 3 left by 1 bit
    if (sim_time % 7 == 3) {
      dut->operator_i = 0b0100111;
      dut->operand_a_i = 3;
      dut->operand_b_i = 1;

      // check previous instruction
      dut->check_op_group_i = 3;
      dut->check_operator_i = 0b0011001;
      dut->check_operand_a_i = -20;
      dut->check_operand_b_i = -10;
      dut->check_result_i = -10;
    }

    // Compare: is 5 < 3?
    if (sim_time % 7 == 4) {
      dut->operator_i = 0b0000001;
      dut->operand_a_i = 5;
      dut->operand_b_i = 3;

      // check previous instruction
      dut->check_op_group_i = 1;
      dut->check_operator_i = 0b0100111;
      dut->check_operand_a_i = 3;
      dut->check_operand_b_i = 1;
      dut->check_result_i = 6;
    }

    if (sim_time % 7 == 5) {
      dut->operator_i = 0b0011000;
      dut->operand_a_i = 10;
      dut->operand_b_i = 5;

      dut->check_op_group_i = 2;
      dut->check_operator_i = 0b0000001;
      dut->check_operand_a_i = 5;
      dut->check_operand_b_i = 3;
      dut->check_result_i = 0;
    }

    // Invert the clock signal to generate edges
    dut->clk ^= 1;

    // Evaluate all signals in the DUT
    dut->eval();

    // Increment the posedge counter
    if (dut->clk == 1) {
      posedge_cnt++;
    }

    // Dump the signal values to the waveform file
    m_trace->dump(sim_time);

    // Increment the cycle counter
    sim_time++;
  }

  // Clean
  m_trace->close();
  delete dut;
  exit(EXIT_SUCCESS);
}
