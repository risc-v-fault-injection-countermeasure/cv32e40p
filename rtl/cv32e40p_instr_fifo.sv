// Based on the implementation found at:
// https://vlsiverify.com/verilog/verilog-codes/synchronous-fifo/
module cv32e40p_instr_fifo
  import cv32e40p_pkg::*;
(
  input  logic clk,
  input  logic rst_n,

  input  instr_group_e op_group_i,
  input  alu_opcode_e operator_i,
  input  logic  [1:0] mul_signed_i,
  input  logic [31:0] operand_a_i,
  input  logic [31:0] operand_b_i,
  input  logic [31:0] result_i,

  output instr_group_e op_group_o,
  output logic  [6:0] operator_o,
  output logic  [1:0] mul_signed_o,
  output logic [31:0] operand_a_o,
  output logic [31:0] operand_b_o,
  output logic [31:0] result_o,

  output logic ready_o, // The FIFO is ready to receive a new instruction
  input  logic valid_i, // The EX stage has a new instruction to write

  input  logic ready_i, // The EX stage is ready to receive a checkable instruction
  output logic valid_o  // The FIFO has a checkable instruction for the EX stage to read
);

localparam int POINTER_WIDTH = $clog2(INSTR_FIFO_DEPTH);
localparam int INSTR_FIFO_WIDTH = INSTR_GROUP_WIDTH + ALU_OP_WIDTH + 2 + 32 + 32 + 32;

logic [POINTER_WIDTH-1:0] write_pointer, read_pointer;
logic [POINTER_WIDTH:0] count;
logic [INSTR_FIFO_DEPTH-1:0][INSTR_FIFO_WIDTH-1:0] data;
logic read, write, empty, full;

assign read = ready_i & valid_o;
assign write = valid_i & ready_o;

assign empty = (count == 0);
assign full = (count == INSTR_FIFO_DEPTH);

// The ready/valid state depends only on whether the FIFO is empty or full
assign ready_o = !full;
assign valid_o = !empty;

always_ff @(posedge clk or negedge rst_n) begin
  if (!rst_n)
    write_pointer <= 0;
  else if (write)
    write_pointer <= write_pointer + 1;
end

always_ff @(posedge clk or negedge rst_n) begin
  if (!rst_n)
    read_pointer <= 0;
  else if (read)
    read_pointer <= read_pointer + 1;
end

always_ff @(posedge clk or negedge rst_n) begin
  if (!rst_n)
    count <= 0;
  else if (read & write)
    count <= count;
  else if (write)
    count <= count + 1;
  else if (read)
    count <= count - 1;
  else
    count <= count;
end

always_ff @(posedge clk or negedge rst_n) begin
  if (!rst_n)
    data <= 0;

  else begin
    if (write) begin
      // Write the current instruction to be checked later
      data[write_pointer[POINTER_WIDTH-1:0]][1:0]    <= op_group_i;
      data[write_pointer[POINTER_WIDTH-1:0]][8:2]    <= operator_i;
      data[write_pointer[POINTER_WIDTH-1:0]][10:9]   <= mul_signed_i;
      data[write_pointer[POINTER_WIDTH-1:0]][42:11]  <= operand_a_i;
      data[write_pointer[POINTER_WIDTH-1:0]][74:43]  <= operand_b_i;
      data[write_pointer[POINTER_WIDTH-1:0]][106:75] <= result_i;
    end

    // Make the first operation accessible even if not read in this cycle
    // (the read pointer is only incremented once for multicycle operations)
    if (valid_o) begin
      // Read a stored operation to be checked in this cycle
      op_group_o   <= instr_group_e'(data[read_pointer[POINTER_WIDTH-1:0]][1:0]);
      operator_o   <= data[read_pointer[POINTER_WIDTH-1:0]][8:2];
      mul_signed_o <= data[read_pointer[POINTER_WIDTH-1:0]][10:9];
      operand_a_o  <= data[read_pointer[POINTER_WIDTH-1:0]][42:11];
      operand_b_o  <= data[read_pointer[POINTER_WIDTH-1:0]][74:43];
      result_o     <= data[read_pointer[POINTER_WIDTH-1:0]][106:75];
    end
  end
end

endmodule
