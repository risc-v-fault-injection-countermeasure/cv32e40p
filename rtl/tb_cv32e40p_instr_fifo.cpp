#include <stdlib.h>
#include <iostream>
#include <verilated.h>
#include <verilated_vcd_c.h>
#include <Vcv32e40p_pkg.h>

// Simulate 100 clock edges
#define MAX_SIM_TIME 100
vluint64_t sim_time = 0;
vluint64_t posedge_cnt = 0;

int main(int argc, char** argv, char** env) {
  Verilated::commandArgs(argc, argv);

  // Instantiate the FIFO module
  Vcv32e40p_pkg *dut = new Vcv32e40p_pkg;

  // Dump waveforms
  Verilated::traceEverOn(true);
  VerilatedVcdC *m_trace = new VerilatedVcdC;
  dut->trace(m_trace, 2);
  m_trace->open("waveform.vcd");

  // Simulate
  while (sim_time < MAX_SIM_TIME) {
    // Reset in cycles 2-5
    if (sim_time > 3 && sim_time <= 6) {
      dut->rst_n = 0;
    } else {
      dut->rst_n = 1;
    }

    // mul 3*4=12
    if (sim_time == 6) {
      dut->op_group_i = 0;
      dut->operator_i = 0;
      dut->mul_signed_i = 0;
      dut->operand_a_i = 3;
      dut->operand_b_i = 4;
      dut->result_i = 12;
      dut->valid_i = 1;
      dut->ready_i = 1;
    }

    // Shift 3 left by 1 bit
    if (sim_time == 8 || sim_time == 12) {
      dut->op_group_i = 1;
      dut->operator_i = 39;
      dut->operand_a_i = 3;
      dut->operand_b_i = 1;
      dut->result_i = 6;
      dut->valid_i = 1;
      dut->ready_i = 1;
    }

    // Add 1+1=2
    if (sim_time == 10 || sim_time == 14) {
      dut->op_group_i = 3;
      dut->operator_i = 24;
      dut->operand_a_i = 1;
      dut->operand_b_i = 1;
      dut->result_i = 2;
      dut->valid_i = 1;
      dut->ready_i = 1;
    }

    // Invert the clock signal to generate edges
    dut->clk ^= 1;

    // Evaluate all signals in the DUT
    dut->eval();

    // Increment the posedge counter
    if (dut->clk == 1) {
      posedge_cnt++;
    }

    // Dump the signal values to the waveform file
    m_trace->dump(sim_time);

    // Increment the cycle counter
    sim_time++;
  }

  // Clean
  m_trace->close();
  delete dut;
  exit(EXIT_SUCCESS);
}
