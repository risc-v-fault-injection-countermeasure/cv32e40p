#include <stdlib.h>
#include <iostream>
#include <verilated.h>
#include <verilated_vcd_c.h>
#include <Vcv32e40p_pkg.h>

// Simulate 100 clock edges
#define MAX_SIM_TIME 100
vluint64_t sim_time = 0;

int main(int argc, char** argv, char** env) {
  Verilated::commandArgs(argc, argv);

  // Instantiate the adder module
  Vcv32e40p_pkg *dut = new Vcv32e40p_pkg;

  // Dump waveforms
  Verilated::traceEverOn(true);
  VerilatedVcdC *m_trace = new VerilatedVcdC;
  dut->trace(m_trace, 2);
  m_trace->open("waveform.vcd");

  // Simulate
  while (sim_time < MAX_SIM_TIME) {
    // 3 + 4 = 7
    if (sim_time % 3 == 0) {
      dut->operator_i = 0b0011000;
      dut->operand_a_i = 3;
      dut->operand_a_neg = -3;
      dut->operand_b_i = 4;
      dut->operand_b_neg = -4;
      dut->bmask = 0;
      dut->vector_mode_i = 0;
      dut->is_subrot_i = 0;
    }

    // 9 - 1 = 8
    if (sim_time % 3 == 1) {
      dut->operator_i = 0b0011001;
      dut->operand_a_i = 9;
      dut->operand_a_neg = -9;
      dut->operand_b_i = 1;
      dut->operand_b_neg = -1;
      dut->bmask = 0;
      dut->vector_mode_i = 0;
      dut->is_subrot_i = 0;
    }

    // -20 - -10 = -10
    if (sim_time % 3 == 2) {
      dut->operator_i= 0b0011001;
      dut->operand_a_i = -20;
      dut->operand_a_neg = ~(-20);
      dut->operand_b_i = -10;
      dut->operand_b_neg = ~(-10);
      dut->bmask = 0;
      dut->vector_mode_i = 0;
      dut->is_subrot_i = 0;
    }

    // Evaluate all signals in the DUT
    dut->eval();

    // Dump the signal values to the waveform file
    m_trace->dump(sim_time);

    // Increment the cycle counter
    sim_time++;
  }

  // Clean
  m_trace->close();
  delete dut;
  exit(EXIT_SUCCESS);
}
